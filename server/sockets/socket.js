const { io } = require('../server');
const { TicketControl } = require('../classes/ticket-control');

const ticketControl = new TicketControl();


io.on('connection', (client) => {
    client.on('sendNewTicket', (data, callback) => {
        let newTicket = ticketControl.nextTicket();
        callback(newTicket);
    });

    client.emit('sendLastTicket', {
        last: ticketControl.lastTicket()
    });

    client.emit('emitLastFour', {
        last: ticketControl.lastTicket(),
        lastFour: ticketControl.getLastFour()
    });

    client.on('onAnswerTicket', (data, callback) => {
        if (!data.box) {
            return callback({
                err: true,
                msg: 'El escritorio (box) es necesario'
            });
        }

        let answerTicket = ticketControl.answerTicket(data.box);
        callback(answerTicket);

        client.broadcast.emit('broadcastLastFour', {
            lastFour: ticketControl.getLastFour()
        });
    });
});