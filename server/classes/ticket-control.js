const fs = require('fs');

class TicketControl {
    constructor() {
        this.last = 0;
        this.today = new Date().getDate();
        this.tickets = [];
        this.lastFour = [];

        let data = require('../data/data.json');

        if (data.today === this.today) {
            this.last = data.last;
            this.tickets = data.tickets;
            this.lastFour = data.lastFour;
        } else {
            this.resetCount();
        }
    }

    lastTicket() {
        return `Ticket ${ this.last }`;
    }

    getLastFour() {
        return this.lastFour;
    }

    answerTicket(box) {
        if (this.tickets.length === 0) {
            return 'No hay tickets';
        }
        let numTicket = this.tickets[0].num;
        this.tickets.shift();
        let answerTicket = new Ticket(numTicket, box);
        this.lastFour.unshift(answerTicket);
        if (this.lastFour.length > 4) {
            this.lastFour.splice(-1, 1);
        }

        this.saveFile();
        return answerTicket;
    }

    nextTicket() {
        this.last++;
        let ticket = new Ticket(this.last, null);
        this.tickets.push(ticket);
        this.saveFile();
        return `Ticket ${ this.last }`;
    }

    resetCount() {
        this.tickets = [];
        this.lastFour = [];
        this.last = 0;
        this.saveFile();
        console.log('# Sistema Reiniciado #');
    }

    saveFile() {
        let jsonData = {
            last: this.last,
            today: this.today,
            tickets: this.tickets,
            lastFour: this.lastFour
        };
        let jsonDataString = JSON.stringify(jsonData);
        fs.writeFileSync('./server/data/data.json', jsonDataString);
    }
}

class Ticket {
    constructor(num, box) {
        this.num = num;
        this.box = box;
    }
}

module.exports = {
    TicketControl
}