// Establece comunicacion
var socket = io();

socket.on('emitLastFour', function(resp) {
    showInScreen(resp);
});

socket.on('broadcastLastFour', function(resp) {
    showInScreen(resp);
    var audio = new Audio('../audio/new-ticket.mp3');
    audio.play();
});

function showInScreen(data) {
    for (let i = 0; i <= data.lastFour.length - 1; i++) {
        $('.screen-ticket').eq(i).text('Ticket ' + data.lastFour[i].num);
        $('.screen-box').eq(i).text('Escritorio ' + data.lastFour[i].box);
    }
}