// Establece comunicacion
var socket = io();

// Escucha evento de conexion
socket.on('connect', function() {
    console.log('# Conectado al Server #');
});

// Escucha evento de desconexion
socket.on('disconnect', function() {
    console.log('(X) Conexion perdida con Server');
});

var searchParams = new URLSearchParams(window.location.search);
if (!searchParams.has('escritorio')) {
    window.location = 'index.html';
    throw new Error('El escritorio es necesario');
}
var box = searchParams.get('escritorio');
$('#boxNumber').text(box);

$('button').on('click', function() {
    socket.emit('onAnswerTicket', { box: box }, function(resp) {
        if (resp.err) {
            return resp.msg;
        }
        if (resp == 'No hay tickets') {
            alert(resp);
            return $('small').text(' | ' + resp);
        }

        $('small').text(resp.num);
    });
});