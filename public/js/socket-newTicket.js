// Establece comunicacion
var socket = io();

// Escucha evento de conexion
socket.on('connect', function() {
    console.log('# Conectado al Server #');
});

// Escucha evento de desconexion
socket.on('disconnect', function() {
    console.log('(X) Conexion perdida con Server');
});

// Escuchar info
socket.on('sendMessage', function(data) {
    console.log(data);
});

// Obtener ultimo ticket
socket.on('sendLastTicket', function(resp) {
    $('#lblNuevoTicket').text(resp.last);
});

$('#btnNewTicket').on('click', function() {
    socket.emit('sendNewTicket', null, function(resp) {
        $('#lblNuevoTicket').text(resp);
    });
});