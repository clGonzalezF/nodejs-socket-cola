let getCurrentTime = () => {
    let d = new Date();
    return `${d.getHours() }:${ d.getMinutes() }:${ d.getSeconds() }`;
}

module.exports = {
    getCurrentTime
}